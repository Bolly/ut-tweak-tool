# Changelog

# [0.5.0](https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.21...v0.5.0) (2020-06-20)


### Bug Fixes

* **eslint:** fix violations for `eslint:recommended` ([920f6ef](https://gitlab.com/myii/ut-tweak-tool/commit/920f6ef24bbde17e1448a67ddbcd0f537f52a00a))
* **eslint:** fix violations for further rules ([b62deff](https://gitlab.com/myii/ut-tweak-tool/commit/b62deffa1ce157ae0329c39fae272c324760c45c))
* **nb.po:** update Norwegian translation ([dc7d1e0](https://gitlab.com/myii/ut-tweak-tool/commit/dc7d1e0aca2ae475745986d2cb92b03b8af08bfd))
* **shell.js:** ensure `remount` errors are displayed ([72a89f8](https://gitlab.com/myii/ut-tweak-tool/commit/72a89f816affc7c90df3548e5a9cea7a9009193d))
* **shellcheck:** fix violation in `MountSetRoRwStatus.sh` ([c857360](https://gitlab.com/myii/ut-tweak-tool/commit/c85736071cf62f20ea31dafd084087e31e09578b))


### Build System

* **clickable.json:** replace deprecated `template` with `builder` ([da5a56f](https://gitlab.com/myii/ut-tweak-tool/commit/da5a56f9d2aeeaa269a92d7a00fa7d58a206b58b))


### Continuous Integration

* **gitlab-ci:** add `commitlint` to `lint` stage ([cc66d62](https://gitlab.com/myii/ut-tweak-tool/commit/cc66d626a16184dd740875b113c95a1dbe189643))
* **gitlab-ci:** add `eslint` to `lint` stage ([ef237a0](https://gitlab.com/myii/ut-tweak-tool/commit/ef237a028e7ffc6b5a26d6c1f17bd24eec2d6a3e))
* **gitlab-ci:** add `shellcheck` to `lint` stage ([23f1ec5](https://gitlab.com/myii/ut-tweak-tool/commit/23f1ec59fc3d0b565370e3557a178029f6d15813))
* **gitlab-ci:** add `yamllint` to `lint` stage ([ead2a53](https://gitlab.com/myii/ut-tweak-tool/commit/ead2a53b24a7d2a51d52d98d683629f33453d334))
* **gitlab-ci:** update version number via. `semantic-release` ([c4d8e1d](https://gitlab.com/myii/ut-tweak-tool/commit/c4d8e1d4b53efeee8015415cb4ab8f38077603d1))


### Features

* **behaviour_tab:** remove `Favorite apps` feature after OTA-12 ([887eee6](https://gitlab.com/myii/ut-tweak-tool/commit/887eee6a89455742d60727d1b59260e540e2384d))
* **semantic-release:** implement for this repo ([74809bf](https://gitlab.com/myii/ut-tweak-tool/commit/74809bf8cda79a4d43acca7270997ebb61fb5b4c))


### Styles

* **gitlab-ci:** reformat to new structure before `semantic-release` ([55afb19](https://gitlab.com/myii/ut-tweak-tool/commit/55afb1956772bd7a87583598f916043ac4a7458d))

---

## [[0.4.21][0.4.21]] (Xenial) - 2020-01-14
### Added
- !89 Updated German translation from [@Danfro](https://gitlab.com/Danfro).

### Fixed
- !90 UbuntuColors to Theme Palette and fix SuruDark [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.20][0.4.20]] (Xenial) - 2020-01-12
### Added
- !86 Updated French translation from [@Anne17](https://gitlab.com/Anne17).
- !87 Complete Portuguese translation from [@bugcrazy](https://gitlab.com/bugcrazy).

---

## [[0.4.19][0.4.19]] (Xenial) - 2019-12-12
### Changed
- (#79) Switch themes without restarting Unity 8.
- (#78) Allow desktop file (app name) to be translatable.
- (#84) Use translated app name in main header.

### Deprecated
- (#82) Favourites: add warning about deprecation after OTA-12.

### Fixed
- (#76) Make About Title wrap to page [@cibersheep](https://gitlab.com/cibersheep).
- (#80) Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

### Build
- (#74) Remove hardcoded armhf dependency [@jonnius](https://gitlab.com/jonnius).
- (#75) Enable building clicks for different architectures [@jonnius](https://gitlab.com/jonnius).

---

## [[0.4.18][0.4.18]] (Xenial) - 2019-09-13
### Fixed
- (#33) Audio: only reset mount to `ro` if it was initially `ro`.
- Replace ampersand with plus sign due to UUITK bug (Apps + Scopes).
- German translation from [@Danfro](https://gitlab.com/Danfro).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

---

## [[0.4.17][0.4.17]] (Xenial) - 2019-07-22
### Fixed
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.16][0.4.16]] (Xenial) - 2019-06-26
### Fixed
- (#20) Prevent cropped pages/text faced when using higher screen scaling values [@cibersheep](https://gitlab.com/cibersheep).
- Improve use of colours and general UI tweaks [@cibersheep](https://gitlab.com/cibersheep).
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.15][0.4.15]] (Xenial) - 2019-06-09
### Fixed
- (#30) Fixed scaling slider to extend beyond beyond lowest and highest default values on current devices.

---

## [[0.4.14][0.4.14]] (Xenial) - 2019-05-26
### Added
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.13][0.4.13]] (Xenial) - 2019-05-22
### Fixed
- Fixed `System > (USB settings) ADB settings`:
  + (#27) `RNDIS` was broken and required the `tethering` command to also be run.
  + Also prevented the commands being run every time the page is opened.

---

## [[0.4.12][0.4.12]] (Xenial) - 2019-04-11
### Added
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- German translation from [@Danfro](https://gitlab.com/Danfro).

---

## [[0.4.11][0.4.11]] (Xenial) - 2019-04-11
### Added
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.10][0.4.10]] (Xenial) - 2019-04-10
### Added
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.9][0.4.9]] (Xenial) - 2019-04-09
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Ensure the UTTT app is killed when redeploying to a device using `clickable`.

### Fixed
- Fixed issues when using the SuruDark theme -- big thanks to [@Fusekai](https://gitlab.com/Fusekai) for identifying and resolving these.

---

## [[0.4.8][0.4.8]] (Xenial) - 2018-12-19
### Added
- (#9) Experimental Unity 8 system theme selector for the three themes shipped by default: Ambiance, SuruDark & SuruGradient -- from `Behavior > System theme`.
- Complete Polish translation from [@Daniel20000522](https://gitlab.com/Daniel20000522).

---

## [[0.4.7][0.4.7]] (Xenial) - 2018-12-09
### Added
- (#11) Added the third option of being able to make the image writable temporarily, until the next reboot -- from `System > Make image writable`.
- (#13) Now able to manually restart Unity 8 (i.e. restart the home screen) -- from `System > Services`.
- German translation from [@Danfro](https://gitlab.com/Danfro) x2!
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Updated links due to the transfer of the repo to the new maintainer.

### Removed
- (#11) Rebooting no longer required when making the image writable.

---

## [[0.4.6][0.4.6]] (Xenial) - 2018-12-07
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

---

## [[0.4.5][0.4.5]] (Xenial) - 2018-12-05
### Added
- (#10) SSH settings: toggle for enabling/disabling SSH.
- Changelog based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed
- Centralised app version number.

---

## [0.4.4] (Xenial) - 2018-11-17
- Updated the German translation

---

## [0.4.3] (Xenial)
- Updated the scale option
- Fix the sound notification
- Other small bugs fixed
- Updating translations

---

## [0.4.2] (Xenial)
- Few bug fixed
- Updating translations
- Adding support to change the SMS tone

---

## [0.4.1] (Xenial)
- Adding support for Morph Browser

---

## [0.3.91]
- Fixed issue with Unity8 settings visibility

---

## [0.3.90]
- Fix charset when reading .desktop files, thanks to Michael Zanetti
- [OTA-14] Added option to enable/disable the indicator menu, thanks to Brian Douglass
- [OTA-14] Added option to enable/disable the launcher, thanks to Brian Douglass
- Re-sorted entries for Unity 8 settings
- Updated translation template

---

[0.4.21]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.20...v0.4.21
[0.4.20]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.19...v0.4.20
[0.4.19]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.18...v0.4.19
[0.4.18]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.17...v0.4.18
[0.4.17]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.16...v0.4.17
[0.4.16]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.15...v0.4.16
[0.4.15]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.14...v0.4.15
[0.4.14]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.13...v0.4.14
[0.4.13]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.12...v0.4.13
[0.4.12]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.11...v0.4.12
[0.4.11]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.10...v0.4.11
[0.4.10]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.9...v0.4.10
[0.4.9]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.8...v0.4.9
[0.4.8]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.7...v0.4.8
[0.4.7]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.6...v0.4.7
[0.4.6]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.5...v0.4.6
[0.4.5]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.4...v0.4.5
